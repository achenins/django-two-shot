from django.urls import path
from receipts.views import show_list, create_receipt, category_list, account_list, create_expensecategories, create_account

urlpatterns = [
    path("", show_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_expensecategories, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
